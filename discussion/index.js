console.log("Za Warudo!");

// JS Objects
// JS Objects is a way to define a real world object with its own characteristics.
// It is also a way to organize data with context throug the use of key-value pairs.

/*
	loyal,
	huggable,
	4 legs,
	furry,
	different colors,
	hyperactive,
	tail,
	breed,
*/

// Create a list, use an array
// let dog = ["loyal",4,"tail","Husky"];

// Describe something with characteristics, use object
let dog = {

	breed: "Husky",
	color: "White",
	legs: 4,
	isHuggable: true,
	isLoyal: true
}

let videoGame = {

	title: "God of War",
	publisher: "Sony",
	year: 2018,
	price: 1200,
	isAvailable: true
}
console.log(videoGame);

// If [] are used to create arrays, what did you use to create your object?
// [] - array literals create arrays
// {} - object literals create object

// Objects are composed of key-value pairs. key provide label to your values.
// key: value
// Each key-value pair together are called properties


// Accessing array items = arrName[index]
// Accessing Object properties = objectName.propertyName

console.log(videoGame.title);

// What if I want to access the publisher of our videoGame?
console.log(videoGame.publisher);

// Can we update the properties of our object?
// object.propertyName = <newValue>
videoGame.price = 2000;
console.log(videoGame);

// Miniactivity
videoGame.title = "Final Fantasy X"
videoGame.publisher = "Square Enix"
videoGame.year = "2001"

console.log(videoGame);

// Objects can not only have primitive values like strings, numbers or boolean
// It can also contain objects and arrays.

let course = {

	title: "Philosophy 101",
	description: "Learn the values of life",
	price: 5000,
	isActive: true,
	instructors: ["Mr. Johnson","Mrs. Smitch","Mr.Francis"]
};
console.log(course);
// Can we access the course's instructors array?
console.log(course.instructors);
// How can we access Mrs. Smith, an instructor from our instructors array?
console.log(course.instructors[1]);
// Can we use the instructor array's array methods?
// How can we delete Mr.Francis from our instructors array?
course.instructors.pop();
console.log(course);

// Mini activity

course.instructors.push("Mr. McGee");
console.log(course.instructors);

let isAnInstructor = course.instructors.includes("Mr. Johnson");
console.log(isAnInstructor);

// Create a function to be able to add new instructors to our object
function addNewInstructor(instructor) {
	
	// true or false
	let isAdded = course.instructors.includes(instructor);

	if(isAdded){
		console.log("Instructor already added.");
	} else{
		course.instructors.push(instructor);
		console.log("Thank you. Instructor added.");
	}

};

addNewInstructor("Mr. Marco");
addNewInstructor("Mr. Smith");
addNewInstructor("Mr. Smith");

console.log(course);

// We can also create/initialize an empty object and then add its properties afterwards

let instructor = {};
console.log(instructor);

instructor.name = "James Johnson";
console.log(instructor);


instructor.job = "Instructor";
instructor.age = "56 years old";
instructor.gender = "male";
instructor.department = "Humanities";
instructor.salary = 50000;
instructor.teaches = ["Philosophy","Humanities","Logic"];

console.log(instructor)

instructor.address = {
	street: "#1 Maginhawa St.",
	city: "Quezon City",
	country: "Philippines"
}
console.log(instructor);
// How will we access the street property of our instructor's address?
console.log(instructor.address.street);

// Create Objects using a constructor function

// Create a reusable function to create objects whose structure and keys are the same. Think of creating a function that serves a blueprint for an object

function Superhero(name,superpower,powerLevel){

	// "this" keyword when added in a constructor function refers to the object that will be made by the function.
	/*
		{
			name: <valueOfParameterName>
			superpower: <valueOfParameterSuperpower>
			powerLevel: <valueOfParameterPowerLevel>
		}
		
	*/
	this.name = name;
	this.superpower = superpower;
	this.powerLevel = powerLevel;
}

// Create an object out of our Superhero constructor function
// new keyword is added to allow us to create a new object out of our function.
let superhero1 = new Superhero("Saitama","One Punch", 30000);
console.log(superhero1);

// Mini activity

function Laptop(name,brand,price){

	this.name = name;
	this.brand = brand;
	this.price = Number(price);
}

let laptop1 = new Laptop("Aspire","Asus","70000");
console.log(laptop1);

let laptop2 = new Laptop("Legion","Lenovo","150000");
console.log(laptop2);

// Object Methods
// Object Methods are functions that are associated with an object.
// A function is a property of an object and that function belongs to the object.
// Methods are tasks that an object can perform or do.

// arrayName.method()

let person = {
	name: "Slim Shady",
	talk: function(){

		// methods are functions associated as a property of an object.
		 // They are anonymous functions we can invoke using the property yof the object.
		 // "this" refers to the object where the methods is associated
		 // console.log(this);
		 console.log("Hi! My name is, What? My name is who? " + this.name); 

	}
}

let person2 = {

	name: "Dr. Dre",
	greet: function(friend){

		// greet() method should be able to receive a single object
		// console.log(friend);
		console.log("Good day, " + friend.name);
	}

}

person.talk();

person2.greet(person)

// Create a constructor with a built-in method

function Dog(name,breed){

/*	{
		name: <valueParameterName>
		breed: <valueParameterBreed>
	}


	*/
	this.name = name;
	this.breed = breed;
	this.greet = function(friend){
		console.log("Bark! bark, " + friend.name);
	}

}

let dog1 = new Dog("Rocky","Bulldog");
console.log(dog1);

dog1.greet(person);
dog1.greet(person2);

let dog2 = new Dog("Blackie", "Rottweiler");
console.log(dog2);

dog2.greet(person);
