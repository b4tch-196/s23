console.log("Za Warudo!!");

let trainer = {}

trainer.name = "Ash Ketchum";
trainer.age = 10;
trainer.pokemon = ["Pikachu","Charizard","Squirtle","Bulbasaur"];
trainer.friends = {

	hoennn: ["May","Max"],
	kanto: ["Brock","Misty"]
	
};

trainer.talk = function(){
	console.log("Pikachu! I choose you!")
}

console.log(trainer);
console.log("Result of dot notation: ");
console.log(trainer.name);
console.log("Result of square bracket notation: ");
console.log(trainer["pokemon"]);

trainer.talk();

function Pokemon(name,level,health,attack){

	this.name = name;
	this.level = Number(level);
	this.health = 3 * level;
	this.attack = 1.5 * level;
	this.tackle = function(pokemon){

		console.log(this.name + " tackled " + pokemon.name + "!");

		let remainingHealth = pokemon.health - this.attack;


		console.log(pokemon.name + '\'s' + " health is now reduced to " + remainingHealth);


		if(remainingHealth <= 0){
			pokemon.faint();
		}
	}

	this.faint = function(){
		console.log(this.name + " has fainted!");
	}

}

let pokemon1 = new Pokemon("Pikachu",12,36,24);
console.log(pokemon1);

let pokemon2 = new Pokemon("Geodude",8,24,21);
console.log(pokemon2);

let pokemon3 = new Pokemon("Mewtwo",100,74,54);
console.log(pokemon3);

pokemon3.tackle(pokemon1);